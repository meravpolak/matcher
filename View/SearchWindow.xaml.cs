﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Matcher
{
    /// <summary>
    /// Interaction logic for SearchWindow.xaml
    /// search for post 
    /// </summary>
    public partial class SearchWindow : Window
    {
        private ArrayList Cities;
        private ArrayList Posts;
        private string city_choose;
        private string domain_choose;
        public Dictionary<int, Post> dic_roomates;
        public Dictionary<int, Post> dic_sport;
        public Dictionary<int, Post> dic_date;
        public Dictionary<int, Post> dic_travel;
        public User m_user;
        // constructor
        public SearchWindow(ArrayList cities, User user)
        {
            InitializeComponent();
            m_user = user;
            dic_roomates = new Dictionary<int, Post>();
            dic_sport = new Dictionary<int, Post>();
            dic_date = new Dictionary<int, Post>();
            dic_travel = new Dictionary<int, Post>();
            Cities = cities;
            city_choose = "";
            domain_choose = "";
            ListDomains.Items.Add("Roommates");
            ListDomains.Items.Add("Sport");
            ListDomains.Items.Add("Travel");
            ListDomains.Items.Add("Date");
            Posts = new ArrayList();
            uploadFromfiles(); //read from files posts
            foreach (string city in Cities)
            {
                ListCities.Items.Add(city);
            }
            if (Cities.Count == 0)
            {
                MessageBox.Show("אין מודעות לפרסום במערכת!");
            }
        }

        //read from files posts
        private void uploadFromfiles()
        {
            string startupPath = System.IO.Directory.GetCurrentDirectory();
            string path_roomates = startupPath + "/roomates";
            string[] files = Directory.GetFiles(path_roomates);
            for (int i = 0; i < files.Count(); i++)
            {
                if (File.Exists(files[i]))
                {
                    FileStream fs = new FileStream(files[i], FileMode.Open, FileAccess.Read);
                    StreamReader sr = new StreamReader(fs);
                    string numOfPost = sr.ReadLine();
                    int j;
                    int num = 0;
                    if (Int32.TryParse(numOfPost, out j))
                    {
                        num = j;
                    }
                    string id = sr.ReadLine();
                    string city = sr.ReadLine();
                    if (!Cities.Contains(city.ToLower()))
                    {
                        Cities.Add(city);
                    }
                    string dateTime = sr.ReadLine();
                    string title = sr.ReadLine();
                    string content = sr.ReadLine();
                    sr.ReadLine(); // @
                    for (int k = 0; k < 5; k++)
                    {
                        content = content + " " + sr.ReadLine();
                    }
                    Post p = new Post(numOfPost, id, city, dateTime, title, content);
                    dic_roomates.Add(num, p);
                    sr.Close();
                    fs.Close();
                }
            }
            // for sport
            string path_sport = startupPath + "/sport";
            string[] files1 = Directory.GetFiles(path_sport);
            for (int i = 0; i < files1.Count(); i++)
            {
                if (File.Exists(files1[i]))
                {
                    FileStream fs = new FileStream(files1[i], FileMode.Open, FileAccess.Read);
                    StreamReader sr = new StreamReader(fs);
                    string numOfPost = sr.ReadLine();
                    int j;
                    int num = 0;
                    if (Int32.TryParse(numOfPost, out j))
                    {
                        num = j;
                    }
                    string id = sr.ReadLine();
                    string city = sr.ReadLine();
                    if (!Cities.Contains(city.ToLower()))
                    {
                        Cities.Add(city);
                    }
                    string dateTime = sr.ReadLine();
                    string title = sr.ReadLine();
                    string content = sr.ReadLine();
                    sr.ReadLine(); // @
                    for (int k = 0; k < 5; k++)
                    {
                        content = content + " " + sr.ReadLine();
                    }
                    Post p = new Post(numOfPost, id, city, dateTime, title, content);
                    dic_sport.Add(num, p);
                    sr.Close();
                    fs.Close();
                }
            }
            // for date
            string path_date = startupPath + "/date";
            string[] files2 = Directory.GetFiles(path_date);
            for (int i = 0; i < files2.Count(); i++)
            {
                if (File.Exists(files2[i]))
                {
                    FileStream fs = new FileStream(files2[i], FileMode.Open, FileAccess.Read);
                    StreamReader sr = new StreamReader(fs);
                    string numOfPost = sr.ReadLine();
                    int j;
                    int num = 0;
                    if (Int32.TryParse(numOfPost, out j))
                    {
                        num = j;
                    }
                    string id = sr.ReadLine();
                    string city = sr.ReadLine();
                    if (!Cities.Contains(city.ToLower()))
                    {
                        Cities.Add(city);
                    }
                    string dateTime = sr.ReadLine();
                    string title = sr.ReadLine();
                    string content = sr.ReadLine();
                    sr.ReadLine(); // @
                    for (int k = 0; k < 5; k++)
                    {
                        content = content + " " + sr.ReadLine();
                    }
                    Post p = new Post(numOfPost, id, city, dateTime, title, content);
                    dic_date.Add(num, p);
                    sr.Close();
                    fs.Close();
                }
            }
            //  for travel
            string path_travel = startupPath + "/travel";
            string[] files3 = Directory.GetFiles(path_travel);
            for (int i = 0; i < files3.Count(); i++)
            {
                if (File.Exists(files3[i]))
                {
                    FileStream fs = new FileStream(files3[i], FileMode.Open, FileAccess.Read);
                    StreamReader sr = new StreamReader(fs);
                    string numOfPost = sr.ReadLine();
                    int j;
                    int num = 0;
                    if (Int32.TryParse(numOfPost, out j))
                    {
                        num = j;
                    }
                    string id = sr.ReadLine();
                    string city = sr.ReadLine();
                    if (!Cities.Contains(city.ToLower()))
                    {
                        Cities.Add(city);
                    }
                    string dateTime = sr.ReadLine();
                    string title = sr.ReadLine();
                    string content = sr.ReadLine();
                    sr.ReadLine(); // @
                    for (int k = 0; k < 5; k++)
                    {
                        content = content + " " + sr.ReadLine();
                    }
                    Post p = new Post(numOfPost, id, city, dateTime, title, content);
                    dic_travel.Add(num, p);
                    sr.Close();
                    fs.Close();
                }
            }
        }

        //for search
        private void submitButton_Click(object sender, RoutedEventArgs e)
        {
            ArrayList avaliable_posts = new ArrayList();
            Dictionary<int, Post> temp_dic = new Dictionary<int, Post>();
            if (domain_choose.ToLower() == "roomates")
            {
                temp_dic = dic_roomates;
            }
            if (domain_choose.ToLower() == "travel")
            {
                temp_dic = dic_travel;
            }
            if (domain_choose.ToLower() == "sport")
            {
                temp_dic = dic_sport;
            }
            if (domain_choose.ToLower() == "date")
            {
                temp_dic = dic_date;
            }

            foreach (int num in temp_dic.Keys)
            {
                if (temp_dic[num].CITY.ToLower() == city_choose)
                    avaliable_posts.Add(temp_dic[num]);
            }

            if (avaliable_posts.Count == 0)
            {
                Posts = new ArrayList();
                MessageBox.Show("!אין מודעות רלוונטיות עבורך");
            }
            else
            {
                Posts = new ArrayList();
                ShowPostWndow ShowPOsts = new ShowPostWndow(avaliable_posts);
                ShowPOsts.ShowDialog();
            }

        }

        // check if checked some domain in list
        private void ListDomains_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (ListDomains.SelectedItem == null)
            {
                MessageBox.Show("!עדיין לא בחרת");
            }
            else domain_choose = ListDomains.SelectedItem.ToString();
        }
        // check if checked some city in list
        private void ListCities_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (ListCities.SelectedItem == null)
            {
                MessageBox.Show("!עדיין לא בחרת");
            }
            else city_choose = ListCities.SelectedItem.ToString();
        }
        // back to menu
        private void BackToMenu_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            Window6 menu = new Window6(Cities, m_user);
            menu.ShowDialog();
        }
        // send request
        private void sentRequest_Click(object sender, RoutedEventArgs e)
        {
            ListCities.Items.Clear();
            this.Close();
            sentRequest w = new sentRequest(dic_roomates, dic_sport, dic_date, dic_travel, m_user, Cities);
            w.ShowDialog();

        }
    }
}
