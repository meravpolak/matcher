﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Matcher
{
    /// <summary>
    /// Interaction logic for Window6.xaml
    /// The window menu
    /// </summary>
    public partial class Window6 : Window
    {
        private ArrayList m_cities;
        public User m_user;
        public Dictionary<string, User> dic_requests;
        // menu window
        public Window6(ArrayList cities, User user)
        {
            InitializeComponent();
            m_user = user;
            m_cities = cities;
            dic_requests = new Dictionary<string, User>();

        }
        //message click
        private void Messages_Click(object sender, RoutedEventArgs e)
        {

        }
        //my post click

        private void MyPosts_Click(object sender, RoutedEventArgs e)
        {

        }

        //publich click
        private void Publishnewpost_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NewPostDomain cnpd = new NewPostDomain(m_cities, m_user);
                this.Close();
                cnpd.Show();
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.ToString());
            }

        }

        //search click
        private void Search_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SearchWindow Search = new SearchWindow(m_cities, m_user);
                this.Close();
                Search.ShowDialog();
            }
            catch (Exception e2)
            {
                MessageBox.Show(e2.ToString());
            }
        }
        //request click
        private void requests_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string startupPath = System.IO.Directory.GetCurrentDirectory();
                string path = startupPath + "/Profiles/" + m_user.ID + "/Messages/MessagesUnRead";
                FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(fs);
                string line = "";
                line = sr.ReadLine();
                if (line == null)
                {
                    MessageBox.Show("אין בקשות במערכת!");
                }
                else
                {
                    while (line != null)
                    {
                        int index = line.IndexOf("/");
                        string id = line.Substring(0, index);
                        line = line.Substring(index + 1);
                        index = line.IndexOf("/");
                        string first_name = line.Substring(0, index);
                        line = line.Substring(index + 1);
                        index = line.IndexOf("/");
                        string last_name = line.Substring(0, index);
                        line = line.Substring(index + 1);
                        index = line.IndexOf("/");
                        string city = line.Substring(0, index);
                        line = line.Substring(index + 1);
                        index = line.IndexOf("/");
                        string age = line.Substring(0, index);
                        line = line.Substring(index + 1);
                        string mail = line;
                        User u = new User(id, first_name, last_name, city, age, mail, "");///////////////
                        dic_requests.Add(id, u);
                        line = sr.ReadLine();
                    }
                    sr.Close();
                    fs.Close();
                    requests_window_show r = new requests_window_show(dic_requests);
                    r.ShowDialog();
                    string startupPath1 = System.IO.Directory.GetCurrentDirectory();
                    string pathToDelete = startupPath1 + "/Profiles/" + m_user.ID + "/Messages/MessagesUnRead";
                    File.Delete(path);
                    // create new
                    FileStream f = new FileStream(pathToDelete, FileMode.Create);
                    fs.Close();
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.ToString());
            }

        }

        //logout click
        private void LogOut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
            }

            catch (Exception e1)
            {
                MessageBox.Show(e1.ToString());
            }
        }
    }
}
