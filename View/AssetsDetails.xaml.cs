﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Matcher
{
    /// <summary>
    /// Interaction logic for Window3.xaml
    /// get the Assets Details
    /// </summary>
    public partial class Window3 : Window
    {
        public string file_pathID;
        public User m_user;
        // constructor
        public Window3(string path_ID, User user)
        {
            InitializeComponent();
            file_pathID = path_ID;
            m_user = user;
        }

        private void submitButton_Click(object sender, RoutedEventArgs e)
        {
            // check if all the fields are full
            if (CityTextBox.Text == "" || StreetTextBox.Text == "" || NumberTextBox.Text == "" || REntTextBox.Text == "" || NumberTextBox.Text == "" || FloorTextBox.Text == "" || SquareMeterTextBox.Text == "")
            {
                System.Windows.MessageBox.Show("!אתה חייב למלא את השדות");
            }
            // all the fields are full
            else
            {
                string city = CityTextBox.Text;
                string street = StreetTextBox.Text;
                string number = NumberTextBox.Text;
                string rent = REntTextBox.Text;
                string numberOfPartners = NumberTextBox.Text;
                string floor = FloorTextBox.Text;
                string squareMeter = SquareMeterTextBox.Text;
                bool elivator = false;
                if (Elivator.IsChecked == true)
                {
                    elivator = true;
                }
                // write all the fields to database
                FileStream fs = new FileStream(file_pathID + "/details", FileMode.Append, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                sw.WriteLine(city + ";" + street + ";" + number + ";" + rent + ";" + numberOfPartners + ";" + floor + ";" + squareMeter + ";" + elivator);
                sw.Close();
                fs.Close();
                this.Close();
                MoreAppartemnts moreAppWIndow = new MoreAppartemnts(file_pathID, m_user);
                moreAppWIndow.ShowDialog();
            }
        }
    }
}
