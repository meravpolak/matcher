﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Matcher
{
    /// <summary>
    /// Interaction logic for requests_window_show.xaml
    /// </summary>
    public partial class requests_window_show : Window
    {
        Dictionary<string, double> m_rank;
        string choose_item;
        Dictionary<string, User> m_dic_requests;

        //request window
        public requests_window_show(Dictionary<string, User> dic_requests)
        {
            m_dic_requests = dic_requests;
            choose_item = "";
            InitializeComponent();
            m_rank = new Dictionary<string, double>();
            requestListBox.Items.Clear();
            foreach (string id in dic_requests.Keys)
            {
                requestListBox.Items.Add("id: " + dic_requests[id].ID);
                requestListBox.Items.Add("name: " + dic_requests[id].FirstNAme);
                requestListBox.Items.Add("last name :" + dic_requests[id].LASTNAME);
                requestListBox.Items.Add("Age :" + dic_requests[id].AGE);
                requestListBox.Items.Add("City :" + dic_requests[id].CITY);
                requestListBox.Items.Add(" ");
                idListBox.Items.Add(dic_requests[id].ID);
            }

        }

        // id list box
        private void idListBox_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (idListBox.SelectedItem == null)
            {
                MessageBox.Show("!עדיין לא בחרת");
            }
            else choose_item = idListBox.SelectedItem.ToString();
        }

        // select button
        private void buttonSelect_Click(object sender, RoutedEventArgs e)
        {
            int i;
            if (RankTextBox.Text != "" && choose_item != "")
            {
                if (Int32.TryParse(RankTextBox.Text, out i))
                {
                    if (i < 0 && i > 5)
                    {
                        MessageBox.Show("עליך לדרג בין 1 ל5 את המועמד!");
                    }
                    else
                    {
                        m_rank[choose_item] = i;
                    }

                }
            }
            else
            {
                MessageBox.Show("לא בחרת משתמש ודירגת אותו!");
            }

        }


        // close button
        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            double max = 0;
            string id = "";
            if (m_rank.Count == m_dic_requests.Count)
            {
                foreach (string t in m_rank.Keys)
                {
                    if (m_rank[t] > max)
                    {
                        max = m_rank[t];
                        id = t;
                    }
                }
                string mail = m_dic_requests[id].MAIL;
                if (max > 0 && max != 1)
                {
                    try
                    {
                        MailMessage mailTosent = new MailMessage();
                        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
                        mailTosent.From = new MailAddress("systempartnermatcher@gmail.com");
                        mailTosent.To.Add(mail);
                        mailTosent.Subject = "Successfully Approved to post";
                        mailTosent.Body = "Hello " + m_dic_requests[id].FirstNAme + " " + m_dic_requests[id].LASTNAME + "Your request is approved ! welcome! ";
                        smtpServer.Port = 587;
                        smtpServer.Credentials = new System.Net.NetworkCredential("systempartnermatcher@gmail.com", "pm12345678");
                        smtpServer.EnableSsl = true;
                        smtpServer.Send(mailTosent);
                        System.Windows.MessageBox.Show("!הבקשה נשלחה למבקש הבקשה");

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
                //
                else
                {
                    try
                    {
                        MailMessage mailTosent = new MailMessage();
                        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
                        mailTosent.From = new MailAddress("systempartnermatcher@gmail.com");
                        mailTosent.To.Add(mail);
                        mailTosent.Subject = "Unfortunally disapproved to post";
                        mailTosent.Body = "Hello " + m_dic_requests[id].FirstNAme + " " + m_dic_requests[id].LASTNAME + "Your request is denied! we are sorry ! ";
                        smtpServer.Port = 587;
                        smtpServer.Credentials = new System.Net.NetworkCredential("systempartnermatcher@gmail.com", "pm12345678");
                        smtpServer.EnableSsl = true;
                        smtpServer.Send(mailTosent);
                        System.Windows.MessageBox.Show("!הבקשה נשלחה למבקש הבקשה");

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }


                foreach (string ids in m_dic_requests.Keys)
                {
                    if (ids != id)
                    {
                        try
                        {
                            MailMessage mailTosent = new MailMessage();
                            SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
                            mailTosent.From = new MailAddress("systempartnermatcher@gmail.com");
                            mailTosent.To.Add(m_dic_requests[ids].MAIL);
                            mailTosent.Subject = "Unfortunally disapproved to post";
                            mailTosent.Body = "Hello " + m_dic_requests[id].FirstNAme + " " + m_dic_requests[id].LASTNAME + "Your request is denied! we are sorry ! ";
                            smtpServer.Port = 587;
                            smtpServer.Credentials = new System.Net.NetworkCredential("systempartnermatcher@gmail.com", "pm12345678");
                            smtpServer.EnableSsl = true;
                            smtpServer.Send(mailTosent);
                            System.Windows.MessageBox.Show("!הבקשה נשלחה למבקש הבקשה");

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                    }

                }
                this.Close();

            }
            else
            {
                MessageBox.Show("לא סיימת לדרג את כל המועמדים!");
            }
        }
    }
}
