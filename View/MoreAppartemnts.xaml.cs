﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Matcher
{
    /// <summary>
    /// Interaction logic for MoreAppartemnts.xaml
    /// more appartments
    /// </summary>
    public partial class MoreAppartemnts : Window
    {
        public string file_Id;
        public User m_user;
        // more appartments
        public MoreAppartemnts(string file, User user)
        {
            InitializeComponent();
            file_Id = file;
            m_user = user;
        }

        // yes there is more assets
        private void YesMoreAssets_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            // open window of more assets
            Window3 MoreAssets = new Window3(file_Id, m_user);
            MoreAssets.ShowDialog();
        }
        // no more assets
        private void NoMoreAssets_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            try
            {
                MailMessage mailTosent = new MailMessage();
                SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
                mailTosent.From = new MailAddress("systempartnermatcher@gmail.com");
                mailTosent.To.Add(m_user.MAIL);
                mailTosent.Subject = "Successfully Signed";
                mailTosent.Body = "Hello " + m_user.FirstNAme + " " + m_user.LASTNAME + " ! You signed succesfully to the system PartnersMatcher!";
                smtpServer.Port = 587;
                smtpServer.Credentials = new System.Net.NetworkCredential("systempartnermatcher@gmail.com", "pm12345678");
                smtpServer.EnableSsl = true;
                smtpServer.Send(mailTosent);
                System.Windows.MessageBox.Show("!נרשמת בהצלחה למערכת");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            System.Windows.MessageBox.Show("!נרשמת בהצלחה למערכת");
        }
    }
}
