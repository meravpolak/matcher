﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Matcher
{
    /// <summary>
    /// Interaction logic for CreateNewPost.xaml
    /// create new post
    /// </summary>
    public partial class CreateNewPost : Window
    {
        int m_postID;
        public User m_user;
        string m_profileID;
        string m_DomainType;
        bool m_LikeToOut = false;
        bool m_LikeToTalk = false;
        bool m_LikeAnimals = false;
        bool m_LikeToSmoke = false;
        bool m_LikeOrginized = false;
        public ArrayList m_cities;

        //create new post
        public CreateNewPost(string domainType, User user, ArrayList cities)
        {
            InitializeComponent();
            m_profileID = user.ID;
            m_user = user;
            m_cities = cities;
            m_DomainType = domainType;
        }

        // click on the submit button
        private void submitButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            string startupPath = System.IO.Directory.GetCurrentDirectory();

            //Assign to postID by the number of posts file
            if (System.IO.Directory.Exists(startupPath + "/" + m_DomainType))
            {
                string[] postsFile = System.IO.Directory.GetFiles(startupPath + "/" + m_DomainType);
                if (postsFile.Length > 0)
                {
                    m_postID = postsFile.Length + 1;
                }
                else
                {
                    System.IO.Directory.CreateDirectory(startupPath + '/' + m_DomainType);
                    m_postID = 1;
                }
            }
            ////First post in DB:
            //else
            //{
            //    System.IO.Directory.CreateDirectory(startupPath + '/' + m_DomainType);
            //    m_postID = 1;
            //}

            //Extra details about the publisher:
            if (LikeToSmoke.HasContent && LikeToSmoke.IsChecked == true)
                m_LikeToSmoke = true;
            if (LikeOrginized.HasContent && LikeOrginized.IsChecked == true)
                m_LikeOrginized = true;
            if (LikeAnimals.HasContent && LikeAnimals.IsChecked == true)
                m_LikeAnimals = true;
            if (LikeToTalk.HasContent && LikeToTalk.IsChecked == true)
                m_LikeToTalk = true;
            if (LikeToOut.HasContent && LikeToOut.IsChecked == true)
                m_LikeToOut = true;

            //Write posts details to database:
            System.IO.FileStream fs = System.IO.File.OpenWrite(startupPath + '/' + m_DomainType + '/' + m_postID);
            System.IO.StreamWriter sw = new System.IO.StreamWriter(fs);
            sw.WriteLine(m_postID);
            sw.WriteLine(m_profileID);
            sw.WriteLine(CityNameTextBox.Text);
            sw.WriteLine(PostDateTextBox.Text);
            sw.WriteLine(PostTitle.Text);
            sw.WriteLine(PostContent.Text);
            sw.WriteLine('@'); //start of extra details
            if (m_LikeToSmoke)
                sw.WriteLine("I'm smoking");
            else sw.WriteLine("I'm not smoking");
            if (m_LikeOrginized)
                sw.WriteLine("I'm orginized");
            else sw.WriteLine("I'm not orginized");
            if (m_LikeAnimals)
                sw.WriteLine("I like animals");
            else sw.WriteLine("I dont like animals");
            if (m_LikeToTalk)
                sw.WriteLine("I like to talk");
            else sw.WriteLine("I dont like to talk");
            if (m_LikeToOut)
                sw.WriteLine("I like to hangout");
            else sw.WriteLine("I dont like to hangout");
            sw.WriteLine('@'); //end of extra details           
            sw.WriteLine('*'); //end of post's details
            sw.Close();
            fs.Close();
            MessageBox.Show("המודעה פורסמה בהצלחה!");
            Window6 menuWindow = new Window6(m_cities, m_user);
            this.Close();
            menuWindow.Show();
        }
    }
}
