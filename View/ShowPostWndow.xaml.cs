﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Matcher
{
    /// <summary>
    /// Interaction logic for ShowPostWndow.xaml
    /// // show the posts
    /// </summary>
    public partial class ShowPostWndow : Window
    {
        // show the posts
        public ShowPostWndow(ArrayList posts)
        {
            InitializeComponent();
            ShowPosts.Items.Clear();
            foreach (Post p in posts)
            {
                ShowPosts.Items.Add("Num_Of_Post :" + p.NUM);
                ShowPosts.Items.Add("date :" + p.DATE);
                ShowPosts.Items.Add("Title :" + p.TITLE);
                ShowPosts.Items.Add("Content :" + p.CONTENT);
                ShowPosts.Items.Add(" ");
            }
        }
    }
}
