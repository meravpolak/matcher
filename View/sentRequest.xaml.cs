﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Matcher
{
    /// <summary>
    /// Interaction logic for sentRequest.xaml
    /// </summary>
    public partial class sentRequest : Window
    {
        private string domain_choose;
        public Dictionary<int, Post> dic_roomates;
        public Dictionary<int, Post> dic_sport;
        public Dictionary<int, Post> dic_date;
        public Dictionary<int, Post> dic_travel;
        public ArrayList m_cities;
        public User m_user;
        public sentRequest(Dictionary<int, Post> roomates, Dictionary<int, Post> sport, Dictionary<int, Post> date, Dictionary<int, Post> travel, User user, ArrayList cities)
        {
            InitializeComponent();
            m_user = user;
            m_cities = cities;
            dic_roomates = roomates;
            dic_sport = sport;
            dic_date = date;
            dic_travel = travel;
            domain_choose = "";
            listBoxZone.Items.Add("roomates");
            listBoxZone.Items.Add("date");
            listBoxZone.Items.Add("sport");
            listBoxZone.Items.Add("travel");
        }

        //send the request
        private void sentPost_Click(object sender, RoutedEventArgs e)
        {
            string numPost = numOdPostTextBox.Text;
            int i;
            int final_num_post;
            if (int.TryParse(numPost, out i))
            {
                string startupPath = System.IO.Directory.GetCurrentDirectory();
                final_num_post = i;
                if (domain_choose == "roomates")
                {
                    if (dic_roomates.ContainsKey(final_num_post))
                    {
                        thepostTextBox.Text = dic_roomates[final_num_post].TITLE + "\n" + dic_roomates[final_num_post].CONTENT;
                        string name_profile = dic_roomates[final_num_post].DOMAIN;
                        string path = startupPath + "/Profiles/" + name_profile + "/MessagesUnRead";
                        FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write);
                        StreamWriter sw = new StreamWriter(fs);
                        sw.WriteLine(m_user.ID + "/" + m_user.FirstNAme + "/" + m_user.LASTNAME + "/" + m_user.CITY + "/" + m_user.AGE + "/" + m_user.MAIL);
                        sw.Close();
                        fs.Close();
                        Window6 menu = new Window6(m_cities, m_user);
                        this.Close();
                        menu.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("אין מודעה כזו במערכת!");
                    }

                }
                if (domain_choose == "sport")
                {
                    if (dic_sport.ContainsKey(final_num_post))
                    {
                        thepostTextBox.Text = dic_sport[final_num_post].TITLE + "\n" + dic_sport[final_num_post].CONTENT;
                        string name_profile = dic_sport[final_num_post].DOMAIN;
                        string path = startupPath + "/Profiles/" + name_profile + "/Messages/MessagesUnRead";
                        FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write);
                        StreamWriter sw = new StreamWriter(fs);
                        sw.WriteLine(m_user.ID + "/" + m_user.FirstNAme + "/" + m_user.LASTNAME + "/" + m_user.CITY + "/" + m_user.AGE + "/" + m_user.MAIL);
                        sw.Close();
                        fs.Close();
                        Window6 menu = new Window6(m_cities, m_user);
                        this.Close();
                        menu.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("אין מודעה כזו במערכת!");
                    }

                }
                if (domain_choose == "date")
                {
                    if (dic_date.ContainsKey(final_num_post))
                    {
                        thepostTextBox.Text = dic_date[final_num_post].TITLE + "\n" + dic_date[final_num_post].CONTENT;
                        string name_profile = dic_date[final_num_post].DOMAIN;
                        string path = startupPath + "/Profiles/" + name_profile + "/Messages/MessagesUnRead";
                        FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write);
                        StreamWriter sw = new StreamWriter(fs);
                        sw.WriteLine(m_user.ID + "/" + m_user.FirstNAme + "/" + m_user.LASTNAME + "/" + m_user.CITY + "/" + m_user.AGE + "/" + m_user.MAIL);
                        sw.Close();
                        fs.Close();
                        Window6 menu = new Window6(m_cities, m_user);
                        this.Close();
                        menu.ShowDialog();
                    }

                    else
                    {
                        MessageBox.Show("אין מודעה כזו במערכת!");
                    }

                }
                if (domain_choose == "travel")
                {
                    if (dic_travel.ContainsKey(final_num_post))
                    {
                        thepostTextBox.Text = dic_travel[final_num_post].TITLE + "\n" + dic_travel[final_num_post].CONTENT;
                        string name_profile = dic_travel[final_num_post].DOMAIN;
                        string path = startupPath + "/Profiles/" + name_profile + "/Messages/MessagesUnRead";
                        FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write);
                        StreamWriter sw = new StreamWriter(fs);
                        sw.WriteLine(m_user.ID + "/" + m_user.FirstNAme + "/" + m_user.LASTNAME + "/" + m_user.CITY + "/" + m_user.AGE + "/" + m_user.MAIL);
                        sw.Close();
                        fs.Close();
                        MessageBox.Show("הבקשה נשלחה בהצלחה!");
                        this.Close();
                        Window6 menu = new Window6(m_cities, m_user);
                        menu.ShowDialog();
                    }

                    else
                    {
                        MessageBox.Show("אין מודעה כזו במערכת!");
                    }

                }
            }
            else
            {
                MessageBox.Show("The num of the post isnt illigal!");
            }
        }




        //list of the zones
        private void listBoxZone_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (listBoxZone.SelectedItem == null)
            {
                MessageBox.Show("!עדיין לא בחרת");
            }
            else
            {
                domain_choose = listBoxZone.SelectedItem.ToString();

            }
        }
    }
}
