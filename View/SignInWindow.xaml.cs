﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Matcher
{
    /// <summary>
    /// Interaction logic for SignInWindow.xaml
    /// sign in window 
    /// </summary>
    public partial class SignInWindow : Window
    {
        //sign in window
        public SignInWindow()
        {
            InitializeComponent();
        }
        // summit click
        private void submitButton_Click(object sender, RoutedEventArgs e)
        {
            // check if the fields are full
            if (FirstNameTextBox.Text == "" || LastNameTextBox.Text == "" || IDTextBox.Text == "" || MailTextBox.Text == "" || TelTextBox.Text == "" || AgeTextBox.Text == "" || AdressTextBox.Text == "" || NewPasswordTextBox.Text == "")
            {
                System.Windows.MessageBox.Show("!חייב למלא את כל השדות");
            }
            else
            {
                string startupPath = System.IO.Directory.GetCurrentDirectory();
                string first_name = FirstNameTextBox.Text;
                string last_name = LastNameTextBox.Text;
                string ID = IDTextBox.Text;
                string Mail = MailTextBox.Text;
                string Tel = TelTextBox.Text;
                string age = AgeTextBox.Text;
                string City = AdressTextBox.Text;
                string password = NewPasswordTextBox.Text;
                bool ownerApp = false;
                string path_folder = startupPath + "/Profiles";
                if (!System.IO.Directory.Exists(path_folder)) // create: Profiles
                {
                    System.IO.Directory.CreateDirectory(path_folder);
                }
                string path_folder_ID = path_folder + "/" + ID;
                if (!System.IO.Directory.Exists(path_folder_ID)) // create: Profiles/ID
                {
                    System.IO.Directory.CreateDirectory(path_folder_ID); // create: Profiles/ID
                }

                string file = path_folder_ID + "/details"; // create: Prifiles/ID/details
                if (!System.IO.Directory.Exists(path_folder_ID + "/Messages")) // create: Profiles/ID/Messages
                {
                    System.IO.Directory.CreateDirectory(path_folder_ID + "/Messages"); // create: Profiles/ID/Messages
                }
                string Message_read = path_folder_ID + "/Messages" + "/MessagesRead";
                FileStream fs1 = new FileStream(Message_read, FileMode.Create);
                fs1.Close();
                string Message_Unread = path_folder_ID + "/Messages" + "/MessagesUnRead";
                FileStream fs2 = new FileStream(Message_Unread, FileMode.Create);
                fs2.Close();
                // update in the database the fields
                FileStream fs = new FileStream(file, FileMode.Create);
                StreamWriter sw = new StreamWriter(fs);
                sw.WriteLine(password);
                sw.WriteLine(ID);
                sw.WriteLine(first_name);
                sw.WriteLine(last_name);
                sw.WriteLine(Mail);
                sw.WriteLine(Tel);
                sw.WriteLine(age);
                sw.WriteLine(City);
                sw.Close();
                fs.Close();
                User user = new User(ID, first_name, last_name, age, City, Mail, Tel);
                // check if it is owner appartments
                if (OwnerAppartment.IsChecked == true)
                {
                    ownerApp = true;
                    this.Close();
                    Window3 assets = new Window3(path_folder_ID, user);
                    assets.ShowDialog();
                }
                // this is no owner appartments
                else
                {
                    this.Close();
                    try
                    {
                        MailMessage mailTosent = new MailMessage();
                        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
                        mailTosent.From = new MailAddress("systempartnermatcher@gmail.com");
                        mailTosent.To.Add(Mail);
                        mailTosent.Subject = "Successfully Signed";
                        mailTosent.Body = "Hello " + first_name + " " + last_name + " ! You signed succesfully to the system PartnersMatcher!";
                        smtpServer.Port = 587;
                        smtpServer.Credentials = new System.Net.NetworkCredential("systempartnermatcher@gmail.com", "pm12345678");
                        smtpServer.EnableSsl = true;
                        smtpServer.Send(mailTosent);
                        System.Windows.MessageBox.Show("!נרשמת בהצלחה למערכת");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }

                }
            }

        }
    }
}
