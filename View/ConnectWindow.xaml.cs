﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Matcher
{
    /// <summary>
    /// Interaction logic for ConnectWindow.xaml
    /// get the connect window
    /// </summary>
    public partial class ConnectWindow : Window
    {
        private ArrayList Cities;
        public User m_user;
        // constructor
        public ConnectWindow(ArrayList cities)
        {
            InitializeComponent();
            Cities = cities;
        }

        private void submitButton_Click(object sender, RoutedEventArgs e)
        {
            // if the field are empty
            if (UserIdTextBox.Text == "" || PasswordTextBox.Text == "")
            {
                MessageBox.Show("!אתה חייב למלא את כל השדות");
            }
            else
            {
                string startupPath = System.IO.Directory.GetCurrentDirectory(); // where to save details
                string userId = UserIdTextBox.Text;
                string Password_user = PasswordTextBox.Text;
                string path_folder_id = startupPath + "/Profiles/" + userId;
                // the profile exist
                if (System.IO.Directory.Exists(path_folder_id)) // create: Profiles
                {

                    FileStream fs = new FileStream(path_folder_id + "/details", FileMode.Open);
                    StreamReader sr = new StreamReader(fs);
                    string password_File = sr.ReadLine();
                    // the password is correct
                    if (Password_user == password_File)
                    {
                        string id_file = sr.ReadLine(); // id user
                        string first_name_file = sr.ReadLine(); // first name user
                        string lastName_file = sr.ReadLine(); // last name user
                        string mail_file = sr.ReadLine(); // mail user
                        string tel_file = sr.ReadLine(); // tel user
                        string age_file = sr.ReadLine(); // age user
                        string city_file = sr.ReadLine(); // city user
                        User user = new User(id_file, first_name_file, lastName_file, age_file, city_file, mail_file, tel_file);
                        m_user = user;
                        this.Close();
                        Window6 SearchWindow = new Window6(Cities, m_user); // open the search window
                        SearchWindow.ShowDialog();
                    }
                    // WRONG password
                    else
                    {
                        MessageBox.Show("!הסיסמה לא נכונה");
                    }
                    sr.Close();
                    fs.Close();
                }

                // the profile isnt exist
                else
                {
                    MessageBox.Show("!הפרופיל לא קיים");
                }

            }

        }

        // get the user
        public User GETUSER
        {
            get { return m_user; }
        }
    }
}
