﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Matcher
{
    /// <summary>
    /// Interaction logic for NewPostDomain.xaml
    /// new post domain
    /// </summary>
    public partial class NewPostDomain : Window
    {
        User m_user;
        ArrayList m_cities;

        //new post domain
        public NewPostDomain(ArrayList cities, User user)
        {
            InitializeComponent();
            m_cities = cities;
            m_user = user;
        }
        // click on date
        private void Dates_Click(object sender, RoutedEventArgs e)
        {
            CreateNewPost cnp = new CreateNewPost("date", m_user, m_cities);
            this.Close();
            cnp.Show();
        }
        //click on roomates
        private void Roomates_Click(object sender, RoutedEventArgs e)
        {
            CreateNewPost cnp = new CreateNewPost("roomates", m_user, m_cities);
            this.Close();
            cnp.Show();
        }

        //click on sport
        private void Sports_Click(object sender, RoutedEventArgs e)
        {
            CreateNewPost cnp = new CreateNewPost("sport", m_user, m_cities);
            this.Close();
            cnp.Show();
        }

        //click on trave;
        private void Travels_Click(object sender, RoutedEventArgs e)
        {
            CreateNewPost cnp = new CreateNewPost("travel", m_user, m_cities);
            this.Close();
            cnp.Show();
        }
    }
}
