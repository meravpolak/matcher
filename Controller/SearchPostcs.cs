﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matcher.Controller
{
    class SearchPostcs
    {
        //search posts
        public SearchPostcs()
        {
            uploadFromfiles();
        }
        //upload from files
        public void uploadFromfiles()
        {
            ArrayList Cities = new ArrayList();
            Dictionary<int, Post> dic_roomates = new Dictionary<int, Post>();
            Dictionary<int, Post> dic_sport = new Dictionary<int, Post>();
            Dictionary<int, Post> dic_date = new Dictionary<int, Post>();
            Dictionary<int, Post> dic_travel = new Dictionary<int, Post>();
            string startupPath = System.IO.Directory.GetCurrentDirectory();
            string path_roomates = startupPath + "/roomates";
            string[] files = Directory.GetFiles(path_roomates);
            for (int i = 0; i < files.Count(); i++)
            {
                if (File.Exists(files[i]))
                {
                    FileStream fs = new FileStream(files[i], FileMode.Open, FileAccess.Read);
                    StreamReader sr = new StreamReader(fs);
                    string numOfPost = sr.ReadLine();
                    int j;
                    int num = 0;
                    if (Int32.TryParse(numOfPost, out j))
                    {
                        num = j;
                    }
                    string id = sr.ReadLine();
                    string city = sr.ReadLine();
                    if (!Cities.Contains(city.ToLower()))
                    {
                        Cities.Add(city);
                    }
                    string dateTime = sr.ReadLine();
                    string title = sr.ReadLine();
                    string content = sr.ReadLine();
                    sr.ReadLine(); // @
                    for (int k = 0; k < 5; k++)
                    {
                        content = content + " " + sr.ReadLine();
                    }
                    Post p = new Post(numOfPost, id, city, dateTime, title, content);
                    dic_roomates.Add(num, p);
                    sr.Close();
                    fs.Close();
                }
            }
            // for sport
            string path_sport = startupPath + "/sport";
            string[] files1 = Directory.GetFiles(path_sport);
            for (int i = 0; i < files1.Count(); i++)
            {
                if (File.Exists(files1[i]))
                {
                    FileStream fs = new FileStream(files1[i], FileMode.Open, FileAccess.Read);
                    StreamReader sr = new StreamReader(fs);
                    string numOfPost = sr.ReadLine();
                    int j;
                    int num = 0;
                    if (Int32.TryParse(numOfPost, out j))
                    {
                        num = j;
                    }
                    string id = sr.ReadLine();
                    string city = sr.ReadLine();
                    if (!Cities.Contains(city.ToLower()))
                    {
                        Cities.Add(city);
                    }
                    string dateTime = sr.ReadLine();
                    string title = sr.ReadLine();
                    string content = sr.ReadLine();
                    sr.ReadLine(); // @
                    for (int k = 0; k < 5; k++)
                    {
                        content = content + " " + sr.ReadLine();
                    }
                    Post p = new Post(numOfPost, id, city, dateTime, title, content);
                    dic_sport.Add(num, p);
                    sr.Close();
                    fs.Close();
                }
            }
            // for date
            string path_date = startupPath + "/date";
            string[] files2 = Directory.GetFiles(path_date);
            for (int i = 0; i < files2.Count(); i++)
            {
                if (File.Exists(files2[i]))
                {
                    FileStream fs = new FileStream(files2[i], FileMode.Open, FileAccess.Read);
                    StreamReader sr = new StreamReader(fs);
                    string numOfPost = sr.ReadLine();
                    int j;
                    int num = 0;
                    if (Int32.TryParse(numOfPost, out j))
                    {
                        num = j;
                    }
                    string id = sr.ReadLine();
                    string city = sr.ReadLine();
                    if (!Cities.Contains(city.ToLower()))
                    {
                        Cities.Add(city);
                    }
                    string dateTime = sr.ReadLine();
                    string title = sr.ReadLine();
                    string content = sr.ReadLine();
                    sr.ReadLine(); // @
                    for (int k = 0; k < 5; k++)
                    {
                        content = content + " " + sr.ReadLine();
                    }
                    Post p = new Post(numOfPost, id, city, dateTime, title, content);
                    dic_date.Add(num, p);
                    sr.Close();
                    fs.Close();
                }
            }
            //  for travel
            string path_travel = startupPath + "/travel";
            string[] files3 = Directory.GetFiles(path_travel);
            for (int i = 0; i < files3.Count(); i++)
            {
                if (File.Exists(files3[i]))
                {
                    FileStream fs = new FileStream(files3[i], FileMode.Open, FileAccess.Read);
                    StreamReader sr = new StreamReader(fs);
                    string numOfPost = sr.ReadLine();
                    int j;
                    int num = 0;
                    if (Int32.TryParse(numOfPost, out j))
                    {
                        num = j;
                    }
                    string id = sr.ReadLine();
                    string city = sr.ReadLine();
                    if (!Cities.Contains(city.ToLower()))
                    {
                        Cities.Add(city);
                    }
                    string dateTime = sr.ReadLine();
                    string title = sr.ReadLine();
                    string content = sr.ReadLine();
                    sr.ReadLine(); // @
                    for (int k = 0; k < 5; k++)
                    {
                        content = content + " " + sr.ReadLine();
                    }
                    Post p = new Post(numOfPost, id, city, dateTime, title, content);
                    dic_travel.Add(num, p);
                    sr.Close();
                    fs.Close();
                }
            }
        }


    }
}
