﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Matcher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ArrayList Array_cities;
        public User m_user;
        public MainWindow()
        {
            try
            {

                InitializeComponent();
                string startupPath = System.IO.Directory.GetCurrentDirectory();
                if (!System.IO.Directory.Exists(startupPath + "/roomates")) // create: Posts
                {
                    System.IO.Directory.CreateDirectory(startupPath + "/roomates");
                }

                if (!System.IO.Directory.Exists(startupPath + "/date")) // create: Posts
                {
                    System.IO.Directory.CreateDirectory(startupPath + "/date");
                }

                if (!System.IO.Directory.Exists(startupPath + "/sport")) // create: Posts
                {
                    System.IO.Directory.CreateDirectory(startupPath + "/sport");
                }

                if (!System.IO.Directory.Exists(startupPath + "/travel")) // create: Posts
                {
                    System.IO.Directory.CreateDirectory(startupPath + "/travel");
                }

                Array_cities = new ArrayList();

                uploadFromfiles();
                //string path_folder_post1 = startupPath + "/sport/1";
                //FileStream fs = new FileStream(path_folder_post1, FileMode.Create);
                //StreamWriter sw = new StreamWriter(fs);
                //sw.WriteLine("1");
                //sw.WriteLine("77777777");
                //sw.WriteLine("Eilat");
                //sw.WriteLine(DateTime.Now.ToShortDateString());
                //sw.WriteLine("Looking for friend to play football with");
                //sw.WriteLine("Hi, Im looking someone to play football with in Eilat twice a week");
                //sw.Close();
                //fs.Close();

                //string path_folder_post2 = startupPath + "/date/2";
                //FileStream fs1 = new FileStream(path_folder_post2, FileMode.Create);
                //StreamWriter sw1 = new StreamWriter(fs1);
                //sw1.WriteLine("2");
                //sw1.WriteLine("12345678");
                //sw1.WriteLine("Herzeliya");
                //sw1.WriteLine(DateTime.Now.ToShortDateString());
                //sw1.WriteLine("Date in Hezeliya with a girl");
                //sw1.WriteLine("Hi, Im a good looking guy, want to go out with a nice lady");
                //sw1.Close();
                //fs1.Close();
                //string path_folder_post3 = startupPath + "/roomates/3";
                //FileStream fs3 = new FileStream(path_folder_post3, FileMode.Create);
                //StreamWriter sw3 = new StreamWriter(fs3);
                //sw3.WriteLine("3");
                //sw3.WriteLine("11111111");
                //sw3.WriteLine("Beer-Sheva");
                //sw3.WriteLine(DateTime.Now.ToShortDateString());
                //sw3.WriteLine("looking for a roommate");
                //sw3.WriteLine("Hi, Im looking for a guy, to enter my appartment in July, no pets allowed");
                //sw3.Close();
                //fs3.Close();
                //Array_cities.Add("Herzeliya");
                //Array_cities.Add("Eilat");
                //Array_cities.Add("Beer-Sheva");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private void uploadFromfiles()
        {
            try
            {
                string startupPath = System.IO.Directory.GetCurrentDirectory();
                string path_roomates = startupPath + "/roomates";
                string[] files = Directory.GetFiles(path_roomates);
                for (int i = 0; i < files.Count(); i++)
                {
                    if (File.Exists(files[i]))
                    {
                        FileStream fs = new FileStream(files[i], FileMode.Open, FileAccess.Read);
                        StreamReader sr = new StreamReader(fs);
                        string numOfPost = sr.ReadLine();
                        string id = sr.ReadLine();
                        string city = sr.ReadLine().ToLower();
                        if (!Array_cities.Contains(city))
                        {
                            Array_cities.Add(city);
                        }
                        sr.Close();
                        fs.Close();
                    }
                }
                // for sport
                string path_sport = startupPath + "/sport";
                string[] files1 = Directory.GetFiles(path_sport);
                for (int i = 0; i < files1.Count(); i++)
                {
                    if (File.Exists(files1[i]))
                    {
                        FileStream fs = new FileStream(files1[i], FileMode.Open, FileAccess.Read);
                        StreamReader sr = new StreamReader(fs);
                        string numOfPost = sr.ReadLine();
                        string id = sr.ReadLine();
                        string city = sr.ReadLine().ToLower();
                        if (!Array_cities.Contains(city))
                        {
                            Array_cities.Add(city);
                        }
                        sr.Close();
                        fs.Close();
                    }
                }
                // for date
                string path_date = startupPath + "/date";
                string[] files2 = Directory.GetFiles(path_date);
                for (int i = 0; i < files2.Count(); i++)
                {
                    if (File.Exists(files2[i]))
                    {
                        FileStream fs = new FileStream(files2[i], FileMode.Open, FileAccess.Read);
                        StreamReader sr = new StreamReader(fs);
                        string numOfPost = sr.ReadLine();
                        string id = sr.ReadLine();
                        string city = sr.ReadLine().ToLower();
                        if (!Array_cities.Contains(city))
                        {
                            Array_cities.Add(city);
                        }
                        sr.Close();
                        fs.Close();
                    }
                }
                //  for travel
                string path_travel = startupPath + "/travel";
                string[] files3 = Directory.GetFiles(path_travel);
                for (int i = 0; i < files3.Count(); i++)
                {
                    if (File.Exists(files3[i]))
                    {
                        FileStream fs = new FileStream(files3[i], FileMode.Open, FileAccess.Read);
                        StreamReader sr = new StreamReader(fs);
                        string numOfPost = sr.ReadLine();
                        string id = sr.ReadLine();
                        string city = sr.ReadLine().ToLower();
                        if (!Array_cities.Contains(city))
                        {
                            Array_cities.Add(city);
                        }
                        sr.Close();
                        fs.Close();
                    }
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.ToString());
            }
        }
        //sign in click
        private void signIn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // open sign in window
                SignInWindow signInWindow = new SignInWindow();
                signInWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        // connect click

        private void connect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // open connect window
                ConnectWindow ConnectWindow = new ConnectWindow(Array_cities);
                ConnectWindow.ShowDialog();
                User m_user = ConnectWindow.GETUSER;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
    }
}
