﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matcher
{
    // object of post
    public class Post
    {
        private string m_title; // title of the post
        private string m_id; // domain of the post
        private string m_city; // city of the post
        private string m_content; // content of the post
        private string m_numOfPost;
        private string m_date;

        // constructor of the post
        public Post(string num, string id, string city, string date, string title, string contnet)
        {
            m_title = title;
            m_id = id;
            m_city = city;
            m_content = contnet;
            m_numOfPost = num;
            m_date = date;
        }

        // title of the post
        public string TITLE
        {
            get { return m_title; }
            set { m_title = value; }
        }
        // domain of the post
        public string DOMAIN
        {
            get { return m_id; }
            set { m_id = value; }
        }
        // city of the post
        public string CITY
        {
            get { return m_city; }
            set { m_city = value; }
        }
        // contect of the post
        public string CONTENT
        {
            get { return m_content; }
            set { m_content = value; }
        }

        //the num of the post
        public string NUM
        {
            get { return m_numOfPost; }
            set { m_numOfPost = value; }
        }

        // get the date of the post
        public string DATE
        {
            get { return m_date; }
            set { m_date = value; }
        }
    }
}
