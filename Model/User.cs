﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matcher
{
    // object of user
    public class User
    {
        private string m_Id;
        private string m_first_name;
        private string m_last_name;
        private string m_age;
        private string m_city;
        private string m_mail;
        private string m_tel;

        // constructor of user
        public User(string id, string fName, string Lname, string Age, string City, string mail, string tel)
        {
            m_Id = id;
            m_first_name = fName;
            m_last_name = Lname;
            m_age = Age;
            m_city = City;
            m_mail = mail;
            m_tel = tel;
        }

        // id of user
        public string ID
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        // first name of user
        public string FirstNAme
        {
            get { return m_first_name; }
            set { m_first_name = value; }
        }

        // last name of user
        public string LASTNAME
        {
            get { return m_last_name; }
            set { m_last_name = value; }
        }

        //age of user
        public string AGE
        {
            get { return m_age; }
            set { m_age = value; }
        }

        // city of user
        public string CITY
        {
            get { return m_city; }
            set { m_city = value; }
        }

        //mail of user
        public string MAIL
        {
            get { return m_mail; }
            set { m_mail = value; }
        }

        //tel of user
        public string TEL
        {
            get { return m_tel; }
            set { m_tel = value; }
        }
    }
}